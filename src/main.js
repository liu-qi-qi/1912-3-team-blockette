import Vue from 'vue'
import App from './App'
import toPage from './utils/toPage'
Vue.config.productionTip = false
Vue.prototype.$toPage = toPage
App.mpType = 'app'

const app = new Vue({
  ...App
})
app.$mount()
