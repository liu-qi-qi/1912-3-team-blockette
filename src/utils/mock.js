import Mock from 'mockjs'

export const getShopCarlist = Mock.Mock('/getShopCarList', {
    "arr|3": {
        actualTotal: null,
        basketDate: "2022-09-13 21:00:36",
        basketId: 73,
        discountId: 0,
        discounts: [],
        distributionCardNo: null,
        oriPrice: 0.01,
        pic: "https://img.mall4j.com/2019/04/de0edd2aaf2d4d3c8b6c3fdfde738805.jpg",
        price: 38,
        prodCount: 1,
        prodId: 70,
        prodName: "【Dole都乐】比利时Truval啤梨12只 进口水果新鲜梨 单果120g左右",
        productTotalAmount: 38,
        shareReduce: null,
        shopId: 1,
        shopName: "mall4j小店1",
        skuId: 399,
        skuName: "",
    }
})