export const formatUrlQueryString = (baseUrl, query) => {
    return Object.keys(query).length ? (baseUrl + '?' + Object.keys(query).map(val => `${val}=${query[val]}`).join('&')) : baseUrl;
}
export default function ({ url = "", query = {}, redirect = false, tabbar = false }) {
    url = formatUrlQueryString(url, query)
    const methodName = tabbar ? 'switchTab' : redirect ? 'redirectTo' : 'navigateTo';
    return new Promise((resolve, reject) => {
        uni[methodName]({
            url,
            success(res) {
                resolve(res)
            },
            fail(error) {
                reject(error)
            }
        })
    })
}