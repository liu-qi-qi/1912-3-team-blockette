import Vue from 'vue'
import { formatUrlQueryString } from './toPage'

const httpTool = ({ timeout, baseUrl, errorHandler, requestHandle }) => {
    const request = async (url, method = 'get', data = {}, header = {}) => {
        const requestObject = {
            url: `${baseUrl}${url}`,
            method,
            data,
            header,
            timeout,
        }
        const config = requestHandle && requestHandle(requestObject)
        if (typeof config === 'object' && config.url && config.method) {
            try {
                const res = uni.request(config)
                return res
            } catch (error) {
                errorHandler && errorHandler(error)
                return Promise.reject(error)
            }
        }
    }
    return {

        get(url, query = {}, header = {}) {
            url = formatUrlQueryString(url, query, header)
            return request(url, "GET", data)

        },

        post(url, data, header = {}) {
            console.log(url, data)
            return request(url, "POST", data, header)
        },

    }

}
const http = httpTool({
    timeout: 10000,
    baseUrl: 'https://bjwz.bwie.com/mall4j',
    requestHandle: (config) => {
        return {
            ...config,
            //添加公共header
            header: {
                'Authorization': wx.getStorageSync('token')
            }
        }
    },
    errorHandler: (error) => { //错误拦截

    }

})

export default http
Vue.prototype.$http = httpTool